import java.util.*;

public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if (liste.isEmpty()){
            return null;
        }
        else {
            Integer minimum = liste.get(0);
            for (Integer i : liste){
                if (i.compareTo(minimum)<0){
                    minimum = i;
                }
            }
            return minimum;
        }
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for (T item : liste){
            if (item.compareTo(valeur) < 1){
                return false;
            }
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2) {
        List<T> liste3 = new ArrayList<>();
        int indice1 = 0;
        int indice2 = 0;
        while (indice1 < liste1.size() && indice2 < liste2.size())
        {
            if (liste1.get(indice1).equals(liste2.get(indice2)))
            {
                if(!liste3.contains(liste1.get(indice1)))
                {liste3.add(liste1.get(indice1));}
                indice1+=1;
                indice2+=1;
            }
            else{
                if (liste1.get(indice1).compareTo(liste2.get(indice2)) > 0)
                    {indice2+=1;}
                else
                    {indice1+=1;}
                }
            }
        return liste3;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        String[] listeString = texte.split(" ");
        ArrayList<String> listeMots = new ArrayList<>();

        for (String mot:listeString){
            if (!mot.equals(""))
                listeMots.add(mot);
        }
        return listeMots;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if (texte.equals("")){
            return null;
        }

        HashMap<String, Integer> dicoMots = new HashMap<>();
        List<String> texteList = decoupe(texte);

        for (String mot : texteList){
            if (dicoMots.containsKey(mot)){
                dicoMots.put(mot, dicoMots.get(mot)+1);
            }
            else{
                dicoMots.put(mot, 1);
            }
        }
        Integer iterMax = 0;
        for (String mot : dicoMots.keySet()){
            if (dicoMots.get(mot).compareTo(iterMax)>0){
                iterMax=dicoMots.get(mot);
            }
        }
        ArrayList<String> listeMotsMax= new ArrayList<>();
        for (String mot : dicoMots.keySet()){
            if (dicoMots.get(mot).equals(iterMax)){
                listeMotsMax.add(mot);
            }
        }
        Collections.sort(listeMotsMax);
        return listeMotsMax.get(0);
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        if (chaine.isEmpty()){
            return true;
        }
        int nbPar = 0;
        for (char c : chaine.toCharArray()){
            if (nbPar<0){
                return false;
            }
            if (c == '(' ){
                nbPar += 1;
            }
            if (c == ')' ){
                nbPar -= 1;
            }
        }
        return nbPar == 0;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        if (chaine.isEmpty())
            return true;
        
        int nbPar=0;
        int nbCro=0;
        ArrayList<String> dernierOuvert=new ArrayList<>();

        for (char c : chaine.toCharArray()){
            if (c == '(' ){
                dernierOuvert.add("P");
                nbPar+=1;
            }
            if (c == ')' ){
                if (dernierOuvert.size()==0 ||!dernierOuvert.get(dernierOuvert.size()-1).equals("P") || nbPar==0){
                    return false;
                }
                nbPar-=1;
                dernierOuvert.remove(dernierOuvert.size()-1);
            }
            if (c == '[' ){
                dernierOuvert.add("C");
                nbCro+=1;
            }
            if (c == ']' ){
                if (dernierOuvert.size()==0 ||!dernierOuvert.get(dernierOuvert.size()-1).equals("C")|| nbCro==0){
                    return false;
                }
                nbCro-=1;
                dernierOuvert.remove(dernierOuvert.size()-1);
            }
        }
        return dernierOuvert.size() == 0;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if (liste.isEmpty()){
            return false;
        }
        int centre;
        int bas=0;
        int haut=liste.size()-1;

        while( bas < haut ){
            centre = ((haut + bas) / 2);
            if (valeur.equals(liste.get(centre))){
                return true; 
            }
            else if(valeur.compareTo(liste.get(centre)) > 0){
                bas = centre + 1;
            }
            else{
                haut = centre;
            }
        }
        return liste.get(haut).equals(valeur);
    }
}
